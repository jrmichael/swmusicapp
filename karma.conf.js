'use strict';

module.exports = function (config) {

    config.set({
        frameworks: ['jasmine'],
        plugins: [
            'karma-phantomjs-launcher',
            'karma-jasmine'
        ],

        files: [
            <!-- bower:js -->
            'bower_components/jquery/dist/jquery.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-animate/angular-animate.js',
            'bower_components/angular-cookies/angular-cookies.js',
            'bower_components/angular-sanitize/angular-sanitize.js',
            'bower_components/angular-ui-router/release/angular-ui-router.js',
            'bower_components/bootstrap/dist/js/bootstrap.js',
            'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            'bower_components/lodash/lodash.js',
            'bower_components/angular-messages/angular-messages.js',
            'bower_components/angular-mocks/angular-mocks.js',
            <!-- endinject -->
            <!-- inject:js -->
            'src/app/index.js',
            'src/components/navbar/navbar.controller.js',
            'src/app/spotify/spotify.module.js',
            'src/app/spotify/spotify.service.js',
            'src/app/spotify/spotify-poster.directive.js',
            'src/app/search/search.controller.js',
            'src/app/main/main.controller.spec.js',
            'src/app/main/main.controller.js',
            'src/app/main/album.controller.js',
            'src/app/common/common.module.js',
            'src/app/common/sw-actual-time.directive.js',
            'src/app/common/paginate.filter.js',
            'src/app/bookmarks/bookmarks.module.js',
            'src/app/bookmarks/bookmarks.service.js',
            'src/app/bookmarks/bookmarks.controller.js',
            <!-- endinject -->
        ],

        browsers: [
            'PhantomJS'
        ],
        autoWatch: false,
        singleRun: true,
        preprocessors: {
            '**/*.html': 'ng-html2js'
        },
        ngHtml2JsPreprocessor: {
            stripPrefix: 'web/main',
            moduleName: 'collector.cached.htmls'
        }
    });

};


