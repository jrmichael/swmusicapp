'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var wiredep = require('wiredep');
var path = require('path');

var paths = gulp.paths;

gulp.task('karma:inject', function () {

    var bowerDeps = gulp.src(wiredep({
        directory: 'bower_components',
        exclude: ['bootstrap-sass-official'],
        dependencies: true,
        devDependencies: true
    }).js);

    var sources = gulp.src(paths.src + '/{app,components}/**/*.js')
        .pipe($.angularFilesort());

    return gulp.src('karma.conf.js')
        .pipe($.inject(bowerDeps, {
            addRootSlash: false,
            transform: toKarmaFile,
            name: 'bower'
        }))
        .pipe($.inject(sources, {
            addRootSlash: false,
            transform: toKarmaFile
        }))
        .pipe(gulp.dest('.'));


    function toKarmaFile(filepath) {
        return '\'' + filepath + '\',';
    }

});

function runTests(singleRun, done) {
    var karma = require('karma').server;
    karma.start({
        configFile: path.join(__dirname, '..', 'karma.conf.js'),
        singleRun: singleRun,
        reporters: ['dots']
    }, done);
}

gulp.task('test', ['karma:inject'], function (done) {
    runTests(true /* singleRun */, done)
});


gulp.task('test:auto', ['karma:inject'], function (done) {
    runTests(false /* singleRun */, done)
});
