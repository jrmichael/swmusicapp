'use strict';

angular.module('musicApp')
    .controller('NavbarCtrl', function ($scope, $state) {
        $scope.date = new Date();

        $scope.items = [
            {state: 'home', label: 'Home'},
            {state: 'search', label: 'Search'},
            {state: 'contact', label: 'Contact'}
        ];

        //$scope.isActive = function (item) {
        //      return item.state === $state.current.name;
        //};

        //<li class="active"><a ui-sref="home">Home</a></li>
        //<li class="menu-item"><a ng-href="#">About</a></li>
        //<li><a ui-sref="search">Search</a></li>
        //<li><a ng-href="#">Contact</a></li>

    });
