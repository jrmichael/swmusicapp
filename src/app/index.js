'use strict';

angular.module('musicApp', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngMessages',
    'ui.router',
    'ui.bootstrap',
    'bookmarks',
    'spotify',
    'common'
])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainCtrl'
            })
            .state('search', {
                url: '/search',
                templateUrl: 'app/search/search.html',
                controller: 'SearchController'
            })
            .state('album', {
                url: '/album/:albumId',
                templateUrl: 'app/main/album.html',
                controller: 'AlbumController'
            })
            .state('contact', {
                url: '/contact',
                templateUrl: 'app/contact/contact.html'
                //controller: 'AlbumController'
            });

        $urlRouterProvider.otherwise('/');
    })
;
