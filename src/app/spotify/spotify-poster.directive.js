angular.module('spotify')

    .directive('spotifyPoster', function () {

        var sizeDict = {
            l: 0,
            m: 1,
            s: 2
        };

        return {
            restrict: 'E',
            template: '<img alt="poster!" ng-src="{{ album.images[s].url }}">',
            scope: {
                album: '=',
                size: '@'
            },
            link: function (scope, element, attrs) {
                scope.s = sizeDict[scope.size];
            }
        };

    });
