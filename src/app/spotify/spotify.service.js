angular.module('spotify')

    .service('spotify', function ($q, $window, $http) {

        var apiUrl = 'https://api.spotify.com/v1/';


        this.getAlbum = function (id) {
            return $http.get(apiUrl + 'albums/' + id)
                .then(function (response) {
                    return response.data;
                })
                .catch(function () {
                    alert('Nie ma albumu o id =', id);
                });
        };

        this.searchAlbum = function (query) {
            var deferred = $q.defer();

            if (!query) {
                deferred.resolve([]);
                return deferred.promise;
            }

            if ($window.localStorage.query === query && $window.localStorage.albums) {
                deferred.resolve(JSON.parse($window.localStorage.albums));
            } else {
                $http.get(apiUrl + 'search?type=album&q=' + query)
                    .then(function (response) {
                        $window.localStorage.query = query;
                        deferred.resolve(response.data.albums.items);
                        $window.localStorage.albums = JSON.stringify(response.data.albums.items);
                    })
                    .catch(function () {
                        deferred.reject('Błąd');
                    });
            }

            return deferred.promise;
        };

    });
