angular.module('musicApp')
    .controller('AlbumController', function ($scope, $stateParams, spotify) {

        console.log($stateParams);

        spotify.getAlbum($stateParams.albumId).then(function (album) {
            $scope.album = album;
        });

    });
