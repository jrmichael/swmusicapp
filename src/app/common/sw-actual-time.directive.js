angular.module('common')

.directive('swActualTime', function ($window, $interval) {
    return {
        restrict: 'E',
        scope: {},
        transclude: true,
        template: '<time datetime="{{ time }}"><ng-transclude></ng-transclude> {{ time | date:"HH:mm:ss" }}</time>',
        link: function (scope) {
            $window.setInterval(function () {
                scope.time = new Date();
                scope.$digest();
                console.log(scope.album);
            }, 1000);
            //$interval(function () {
            //    scope.time = new Date();
            //}, 1000);
        }
    };
});
