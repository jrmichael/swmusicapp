angular.module('bookmarks')

    .factory('bookmarks', function ($window) {

        var bookmarks = load();

        function add(item) {
            bookmarks.push(item);
            save();
        }

        function remove(item) {
            bookmarks = _.without(bookmarks, item);
            save();
        }

        function list() {
            return bookmarks;
        }

        function exists(item) {
            return bookmarks.some(function (bookmark) {
                return bookmark.id === item.id;
            });
        }

        function save() {
            $window.localStorage.bookmarks = JSON.stringify(bookmarks);
        }

        function load() {
            if ($window.localStorage.bookmarks) {
                return JSON.parse($window.localStorage.bookmarks);
            }
            return [];
        }

        return {
            add: add,
            list: list,
            remove: remove,
            exists: exists
        }

    });
