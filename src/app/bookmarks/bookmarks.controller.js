angular.module('bookmarks')

.controller('bookmarksController', function($scope, bookmarks) {

        $scope.bookmarks = bookmarks.list;

        $scope.remove = bookmarks.remove;

    });
