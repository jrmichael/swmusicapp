angular.module('musicApp')

    .controller('SearchController', function ($scope, $window, spotify, bookmarks) {

        var cachedQuery = $window.localStorage.query;
        $scope.query = cachedQuery;

        $scope.search = function (query) {
            $scope.isSearching = true;
            spotify.searchAlbum(query)
                .then(function (albums) {
                    $scope.albums = albums;
                    $scope.isSearching = false;
                })
                .catch(function (reason) {
                    alert(reason);
                });
        };

        $scope.saveBookmark = bookmarks.add;
        $scope.isBookmarked = bookmarks.exists;

        if (cachedQuery) {
            $scope.search(cachedQuery);
        }

        $scope.$watch('query', function () {
            $scope.search($scope.query);
        });

    });
